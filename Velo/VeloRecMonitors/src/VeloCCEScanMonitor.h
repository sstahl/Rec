#ifndef VELOCCESCANMONITOR_H 
#define VELOCCESCANMONITOR_H 1

// Include files
#include "GaudiAlg/GaudiAlgorithm.h"
#include "VeloEvent/VeloTELL1Data.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackInterfaces/IVeloClusterPosition.h"
#include "Kernel/IVeloCCEConfigTool.h"

/** @class VeloCCEScanMonitor
 * 
 * Class for Velo CCE scan monitoring ntuple
 *  @author Dermot Moran, Jon Harrison, Shanzhen Chen
 *  @date   16-05-2016
 */                 
                                                           
namespace Velo
{
  class VeloCCEScanMonitor : public GaudiTupleAlg {
                                                                             
  public:
                                                                             
    typedef IVeloClusterPosition::Direction Direction;

    /** Standard construtor */
    VeloCCEScanMonitor( const std::string& name, ISvcLocator* pSvcLocator );
                                                                             
    /** Destructor */
    virtual ~VeloCCEScanMonitor();

    /** Algorithm execute */
    virtual StatusCode execute();

    /** Algorithm initialize */
    virtual StatusCode initialize();

  private:
    void fillCFEtuple(const LHCb::Track& track);
  
  private:
  
    IVeloClusterPosition* m_clusterTool;
    ITrackExtrapolator* m_linExtrapolator;
    IVeloCCEConfigTool* m_cceTool;
    LHCb::VeloClusters* m_rawClusters;
    std::string m_tracksInContainer;    ///< Input Tracks container location
    std::string m_recordLoc; 
    LHCb::VeloTELL1Datas* m_recorded;   //// ADC samples after saturation
    const DeVelo* m_veloDet ;
    bool m_CenStripOK;

    // resolution study:
    double m_unbiasedResid;
    double m_chi2_unbiasedres;
    double m_interStripFr;
    int    m_sizeCluster2;
    double m_projAngle;

  protected:

    unsigned int m_evt;
    bool checkBCList(unsigned int CheckStrip);
    Gaudi::XYZPoint extrapolateToZ(const LHCb::Track *track, double toZ);
    unsigned int m_testsensor;
    LHCb::Tracks* m_tracks;
    double m_numstripsCCE;
    double m_numstripsCFE;
    int m_ClusThres;
    Direction localTrackDirection(const Gaudi::XYZVector& globalTrackDir,
                                  const DeVeloSensor* sensor) const;
    double projAngleR(const Direction& locDirection, const Gaudi::XYZPoint& aLocPoint);
    double projAnglePhi(const Direction& locDirection,
                        const DeVeloPhiType* phiSensor,
                        unsigned int centreStrip);
    double angleOfInsertion(const Direction& localSlopes,
                            Gaudi::XYZVector& parallel2Track) const;
    std::string m_killSensorList;
    std::vector<int> m_killSensorListVec;
    std::vector<int> m_badStripList;
    int m_CCEstep;
    int m_Voltage;
  };
}
#endif // VELOCCESCANMONITOR_H
