
#include <iostream>
#include <cmath>
#include <map>
#include <sstream>
#include <algorithm>

// local
#include "RichHPDImageAnalysis/LogZHPDImage.h"

using namespace Rich::HPDImage;

//-----------------------------------------------------------------------------
// Implementation file for class : LogZ
//
// 2011-03-07 : Chris Jones
//-----------------------------------------------------------------------------

std::shared_ptr<TH2D> LogZ::filter() const
{
  // construct unique ID each time ...
  // nonsense to keep ROOT happy
  static unsigned long long iH(0);
  std::ostringstream id;
  id << m_inHist->GetName() << "_LogZ_" << ++iH;
  
  // make a new histogram
  auto lzH = std::make_shared<TH2D>
    ( id.str().c_str(),
      (std::string(m_inHist->GetTitle())+" | LogZ").c_str(),
      m_inHist->GetNbinsX(),
      m_inHist->GetXaxis()->GetXmin(),
      m_inHist->GetXaxis()->GetXmax(),
      m_inHist->GetNbinsY(),
      m_inHist->GetYaxis()->GetXmin(),
      m_inHist->GetYaxis()->GetXmax() );
  lzH->GetXaxis()->SetTitle( m_inHist->GetXaxis()->GetTitle() );
  lzH->GetYaxis()->SetTitle( m_inHist->GetYaxis()->GetTitle() );
  
  // loop over bins and fill
  for ( int i = 0; i < m_inHist->GetNbinsX(); ++i )
  {
    for ( int  j = 0; j < m_inHist->GetNbinsY(); ++j )
    {
      const auto cont = m_inHist->GetBinContent(i+1,j+1);
      lzH->Fill( i, j, cont > 0 ? std::log10(cont) : 0.0 );
    }
  }
  
  // return
  return lzH;
}
