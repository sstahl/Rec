
#ifndef RICHHPDIMAGEANALYSIS_LogZIMAGE_H
#define RICHHPDIMAGEANALYSIS_LogZIMAGE_H 1

#include <list>
#include <memory>

#include "TH2D.h"

namespace Rich
{
  namespace HPDImage
  {

    /** @class Clean RichHPDImageAnalysis/LogZ.h
     *
     *  Creates a Log(z) version of the given histogram
     *  
     *  @author Chris Jones
     *  @date   2011-03-07
     */
    class LogZ
    {

    public:

      /// Standard constructor
      LogZ( const TH2D& inH ) : m_inHist(&inH) { }

      /// Destructor
      ~LogZ( ) = default;

    public:

      /// Access the input histogram
      const TH2D* input() const { return m_inHist; }
      
      /// Run the filter
      std::shared_ptr<TH2D> filter() const;
   
    private:

      /// Pointer to original histogram
      const TH2D* m_inHist = nullptr;

    };

  }
}

#endif // RICHHPDIMAGEANALYSIS_LogZIMAGE_H
