
#ifndef RICHHPDIMAGEANALYSIS_HPDFIT_H
#define RICHHPDIMAGEANALYSIS_HPDFIT_H 1

// Local
#include "RichHPDImageAnalysis/HPDPixel.h"

// ROOT
#include "TH2D.h"

// RichKernel
#include "RichKernel/RichDAQDefinitions.h"

// STL
#include <memory>

namespace Rich
{
  namespace HPDImage
  {

    /** @class HPDFit RichHPDImageAnalysis/RichHPDFit.h
     *
     *  Simple class to do a fit to the image in an HPD
     *
     *  @author Chris Jones
     *  @date   02/03/2011
     */
    class HPDFit
    {

    public:

      /** @class Params RichHPDImageAnalysis/RichHPDFit.h
       *
       *  Fit parameters for HPDFit
       *
       *  @author Chris Jones
       *  @date   02/03/2011
       */
      class Params
      {
      public:
        /// Default Constructor
        Params() { }
      public:
        std::string type = "Sobel";          ///< Fit type
        bool cleanHistogram{true};           ///< Flag to turn on image cleaning prior to the fit
        double maxImageShift{3.0};           ///< Max allowed image shift for a good fit
        double maxXYErr{99999};              ///< Max allowed x,y error
        double maxXYErrDeviation{99999};     ///< HPD image fit max errors max deviation
        double maxImageSDFrac{0.5};          ///< Max value of image S.D. / mean
        bool retryWithLogzImage{false};      ///< Retry the fit as needed with a log-z image
        bool retryWithSCurveNormImage{true}; ///< Retry the fit as needed with an S Curve normalised image
      public:
        /// Overload output to ostream
        friend inline std::ostream& operator << ( std::ostream& os,
                                                  const Params & params )
        { return params.fillStream(os); }
      private:
        /// Print this object
        std::ostream& fillStream( std::ostream& os ) const;
      };

      /** @class Params RichHPDImageAnalysis/RichHPDFit.h
       *
       *  Fit result for HPDFit
       *
       *  @author Chris Jones
       *  @date   02/03/2011
       */
      class Result
      {
      public:
        /// Default Constructor
        Result() = default;
        /// Constructor from a histogram pointer
        Result( const TH2D& hist ) { setOriginalHist(hist); }
      public:
        /// Set the fit status
        void setOK( const bool OK ) noexcept { m_OK = OK; }
        /// Access the fit status
        bool OK()             const noexcept { return m_OK; }
      public:
        /// Set the 'SCurve Normalised' flag
        void setUsedSCurveNorm( const bool flag ) noexcept { m_usedSCurveNorm = flag; }
        /// Access the 'SCurve Normalised' flag
        bool usedSCurveNorm()               const noexcept { return m_usedSCurveNorm; }
        /// Set the 'Used log-z' flag
        void setUsedLogZ( const bool flag )       noexcept { m_usedLogZ = flag; }
        /// Access the 'Used log-z' flag
        bool usedLogZ()                     const noexcept { return m_usedLogZ; }
      public:
        /// Set the row number for the centre point and error
        void setRowAndErr( const double val, const double err ) noexcept { m_row = val; m_rowErr = err; }
        /// Set the column number for the centre point and error
        void setColAndErr( const double val, const double err ) noexcept { m_col = val; m_colErr = err; }
        /// Set the image radius and error
        void setRadAndErr( const double val, const double err ) noexcept { m_rad = val; m_radErr = err; }
      public:
        /// Access the row number for the centre point
        double row()         const noexcept { return m_row;    }
        /// Access the row number error for the centre point
        double rowErr()      const noexcept { return m_rowErr; }
        /// Access the column number for the centre point
        double col()         const noexcept { return m_col;    }
        /// Access the column number error for the centre point
        double colErr()      const noexcept { return m_colErr; }
        /// Access the image radius (in pixel units)
        double radInPix()    const noexcept { return m_rad;    }
        /// Access the image radius error (in pixel units)
        double radErrInPix() const noexcept { return m_radErr; }
      public:
        /// Access the x shift in the image centre position from the centre of the silicon sensor
        double x()    const { return -1.0 * ( m_col*Rich::DAQ::PixelSize + 
                                              0.5*Rich::DAQ::PixelSize - 
                                              0.25*Rich::DAQ::NumPixelColumns ); }
        /// Access the error in the x shift in the image centre position from the centre of the silicon sensor
        double xErr() const { return m_colErr * Rich::DAQ::PixelSize; }
        /// Access the y shift in the image centre position from the centre of the silicon sensor
        double y()    const { return -1.0 * ( 0.25*Rich::DAQ::NumPixelColumns - 
                                              m_row*Rich::DAQ::PixelSize - 
                                              0.5*Rich::DAQ::PixelSize ); }
        /// Access the error in the y shift in the image centre position from the centre of the silicon sensor
        double yErr() const { return m_rowErr * Rich::DAQ::PixelSize; }
        /// Access the radius of the image in mm 
        double radInMM()    const { return m_rad    * Rich::DAQ::PixelSize; }
        ///Access the error on the radius of the image in mm 
        double radErrInMM() const { return m_radErr * Rich::DAQ::PixelSize; }
      public:
        /// Access the original histogram
        const std::shared_ptr<TH2D>& originalHist()  const noexcept { return m_originalHist; }
        /// Access the processed histogram
        const std::shared_ptr<TH2D>& processedHist() const noexcept { return m_processedHist; }
        /// Access the fitted (sobel filtered) histogram
        const std::shared_ptr<TH2D>& fittedHist()    const noexcept { return m_fittedHist; }
      public:
        /// Set the original histogram. 
        void setOriginalHist( const TH2D & h ) 
        {
          // Set the original histogram pointer, taking an owned cloned copy
          // as we cannot be sure how long h is going to last after this call.
          m_originalHist = std::make_shared<TH2D>(h);
          // Set the processed histogram
          setProcessedHist( m_originalHist ); 
        }
        /// Set the processed histogram
        template< class HIST >
        void setProcessedHist( const HIST & h )
        {
          // Set the processed histogram
          m_processedHist = h; 
          // set the fitted histogram as the same for now
          setFittedHist(h); 
        }
        /// Set the fitted histogram
        template< class HIST >
        void setFittedHist( const HIST & h ) { m_fittedHist = h; }
      public:
        /// Overload output to ostream
        friend inline std::ostream& operator << ( std::ostream& os,
                                                  const Result & result )
        { return result.fillStream(os); }
      private:
        /// Print this object
        std::ostream& fillStream( std::ostream& os ) const;
      private:
        bool m_OK{false};       ///< Fit status
        double m_row{0};        ///< Row number of centre point
        double m_rowErr{0};     ///< Error on the row number of centre point
        double m_col{0};        ///< Column number of centre point
        double m_colErr{0};     ///< Error on the column number of centre point
        double m_rad{0};        ///< Image radius in pixel units
        double m_radErr{0};     ///< Error on the image radius in pixel units
        bool m_usedLogZ{false}; ///< Used Log-z image
        bool m_usedSCurveNorm{false}; ///< Used SCurve normalised image
      private:
        /// Pointer to the original histogram
        std::shared_ptr<TH2D> m_originalHist;
        /// The processed (cleaned etc.) histogram prior to the Sobel filtering
        std::shared_ptr<TH2D> m_processedHist;
        /// The histogram used for the fit, including and Sobel filters etc.
        std::shared_ptr<TH2D> m_fittedHist;
      };

    public:

      /// Default contructor
      HPDFit();
      
      /// Destructor
      ~HPDFit() = default;
      
    public:

      /// Run a fit of the given 2D HPD image histogram
      HPDFit::Result fit ( const TH2D& hist,
                           const Params& params,
                           const unsigned int nEvents = 0 ) const;

      /// Get the boundary pixels (read only access)
      const Pixel::List & boundaryPixels() const { return m_boundaryPixels; }

    public:

      /// Check the fit errors for strange deviations
      inline bool errorsOK( const HPDFit::Result& result,
                            const Params& params ) const
      {
        // if the fit used log_z image scale expected max error...
        const auto maxErr = 
          ( result.usedLogZ() ? 10.0 * params.maxXYErr : params.maxXYErr );
        return ( result.xErr() < maxErr &&
                 result.yErr() < maxErr &&
                 !asymErrors(result,params) );
      }

    private:

      /// Check for a large asymmetry in the (x,y) errors
      inline bool asymErrors( const HPDFit::Result& result,
                              const Params& params ) const
      {
        const auto avErr = ( result.xErr() + result.yErr() ) / 2.0;
        return ( fabs( result.xErr() - avErr ) > avErr*params.maxXYErrDeviation ||
                 fabs( result.yErr() - avErr ) > avErr*params.maxXYErrDeviation );
      }

      /// image intensity S.D. / mean
      double imageIntensitySDFrac( const HPDFit::Result& result ) const;

    private:

      /// List of boundary pixels
      mutable Pixel::List m_boundaryPixels;

    };

  }
}

#endif // RICHHPDIMAGEANALYSIS_HPDFIT_H
